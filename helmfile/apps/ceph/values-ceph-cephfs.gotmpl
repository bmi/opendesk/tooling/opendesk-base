{{/*
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
---
csiConfig:
  - clusterID: {{ .Values.persistence.ceph.clusterID }}
    monitors: {{ .Values.persistence.ceph.monitors }}

nodeplugin:
  imagePullSecrets:
    {{- range .Values.global.imagePullSecrets }}
    - name: {{ . }}
    {{- end }}
  registrar:
    image:
      repository: {{ .Values.global.imageRegistry }}/sig-storage/csi-node-driver-registrar
  plugin:
    image:
      repository: {{ .Values.global.imageRegistry }}/cephcsi/cephcsi

provisioner:
  imagePullSecrets:
    {{- range .Values.global.imagePullSecrets }}
    - name: {{ . }}
    {{- end }}
  provisioner:
    image:
      repository: {{ .Values.global.imageRegistry }}/sig-storage/csi-provisioner
  resizer:
    image:
      repository: {{ .Values.global.imageRegistry }}/sig-storage/csi-resizer
  snapshotter:
    image:
      repository: {{ .Values.global.imageRegistry }}/sig-storage/csi-snapshotter

storageClass:
  name: "{{ .Values.persistence.storageClassNames.RWX }}"
  clusterID: {{ .Values.persistence.ceph.clusterID }}
  fsName: {{ .Values.persistence.ceph.fsName }}

secret:
  adminID: {{ .Values.persistence.ceph.cephfs.adminID }}
  adminKey: {{ env "CEPH_SECRET_CEPHFS_ADMIN_KEY" }}
...
